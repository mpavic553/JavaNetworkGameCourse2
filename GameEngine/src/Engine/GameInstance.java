/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Engine;

import Enums.GameState;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.UUID;

/**
 *
 * @author Pig3on
 */
public class GameInstance implements Serializable {

    LinkedList<Player> playerList;
    private int MAX_PLAYERS = 2;

    private Player winnner;
    private GameState gameState;
    private UUID gameInstanceID;
    
    private Question question;
    
    
    
    public GameState getGameState() {
        return gameState;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }

    public UUID getGameInstanceID() {
        return gameInstanceID;
    }

    public void setGameInstanceID(UUID gameInstanceID) {
        this.gameInstanceID = gameInstanceID;
    }

    public GameInstance(Player player1) {
        playerList = new LinkedList<>();
        playerList.add(player1);
        this.gameInstanceID = UUID.randomUUID();
    }

    public void setPlayer(Player p) {

        for (int i = 0; i < playerList.size(); i++) {

            if (p.getPlayerID().equals(playerList.get(i).getPlayerID())) {

                playerList.set(i, p);
                return;
            }
        }
        playerList.add(p);
    }

    public LinkedList<Player> getPlayerList() {
        return playerList;
    }

    public int getMAX_PLAYERS() {
        return MAX_PLAYERS;
    }

    public Player getWinnner() {
        return winnner;
    }

    public void setWinnner(Player winnner) {
        this.winnner = winnner;
    }

    public void setMAX_PLAYERS(int MAX_PLAYERS) {
        this.MAX_PLAYERS = MAX_PLAYERS;
    }

    @Override
    public String toString() {
        StringBuilder build = new StringBuilder();

        build.append(gameInstanceID);
        build.append("(Host is: ");
        build.append(playerList.getFirst().getUsername());
        build.append(" ( ");
        build.append(playerList.getFirst().getPlayerID());
        build.append(" ) ");
        build.append(" )");

        return build.toString();

    }

}
