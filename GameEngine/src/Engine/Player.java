/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Engine;

import java.io.Serializable;
import java.util.UUID;

/**
 *
 * @author Pig3on
 */
public class Player implements Serializable {

    private final UUID playerID;
    private final String Username;
    private Solution playerSolution;
    String description;

    public Player(String Username) {
        this.Username = Username;
        playerID = UUID.randomUUID();
    }

    public String getUsername() {
        return Username;
    }

    public Solution getPlayerSolution() {
        return playerSolution;
    }

    public void setPlayerSolution(Solution playerSolution) {
        this.playerSolution = playerSolution;
    }

    public UUID getPlayerID() {
        return playerID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
