/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Engine;

import java.io.Serializable;
import java.time.LocalTime;
import javafx.scene.input.KeyCode;

/**
 *
 * @author Pig3on
 */
public class Move implements Serializable {

    private final KeyCode move;
    private final LocalTime timeOfMove;

    public Move(KeyCode move, LocalTime timeOfMove) {
        this.move = move;
        this.timeOfMove = timeOfMove;
    }

}
