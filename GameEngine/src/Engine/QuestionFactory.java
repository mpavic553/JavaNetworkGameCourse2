/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Engine;

import java.io.File;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.LinkedList;
import javax.naming.NamingException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author Pig3on
 */
public class QuestionFactory {
    
   private File questionsXML;
   private  LinkedList<Question>questions = new LinkedList<>();
   private SecureRandom rng = new SecureRandom();
   
   
   
    public QuestionFactory() throws ParserConfigurationException, SAXException, IOException, NamingException {
        
        questionsXML = JNDI.JNDImpl.getFile("Questions.xml");
       questions =  xmlParse.XMLParserImpl.getQuestionsFromXMl(questionsXML);
       
        System.out.println(questions.size() + " Questions loaded. Ready");
    }
    
   
   
   
   public Question getRandomQuestion(){
       int RandomQuestionIndex = rng.nextInt(questions.size());
       
       return questions.get(RandomQuestionIndex);
       
   }
}
