/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DML;

import CustomExceptions.AbandonedGameException;
import CustomExceptions.GameDrawException;
import CustomExceptions.LobbyFullException;
import CustomExceptions.NoSuchGameInstanceException;
import Engine.GameInstance;
import Engine.Player;
import GameNetwork.NetworkSolutionHandling;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.LinkedList;
import java.util.UUID;

/**
 *
 * @author Pig3on
 */
public class NetworkHandler implements NetworkSolutionHandling {

    private Registry serverRegistry;
    private NetworkSolutionHandling handler;
    private static final String GAME_HOSTNAME = "localhost";
    private static final int GAME_PORT = 5532;
    private static final String SERVER_RMI_SOLUTION_NAME = "gameNetworkImpl";
    private static final int GAME_SOCKET_PORT = 2355;
    private static final String SOCKET_SERVER_ADRESS = "127.0.0.1";

    public NetworkHandler() throws RemoteException, NotBoundException {

        getHandler();

    }

    private void getHandler() throws RemoteException, NotBoundException {
        serverRegistry = LocateRegistry.getRegistry(GAME_HOSTNAME, GAME_PORT);

        handler = (NetworkSolutionHandling) serverRegistry.lookup(SERVER_RMI_SOLUTION_NAME);
    }

    @Override
    public GameInstance startGame(Player p, UUID gameInstanceID) throws RemoteException, NotBoundException, LobbyFullException {

        GameInstance inst = handler.startGame(p, gameInstanceID);

        return inst;
    }

    @Override
    public boolean isOpponentReady(UUID gameInstanceID) throws RemoteException, NotBoundException, AbandonedGameException, NoSuchGameInstanceException {

        return handler.isOpponentReady(gameInstanceID);

    }

    @Override
    public void sendSolution(UUID gameInstanceID, Player p) throws RemoteException, AbandonedGameException {
        handler.sendSolution(gameInstanceID, p);

    }

    public GameInstance getNextGameInsance() throws IOException, ClassNotFoundException {

        GameInstance i;
        try (Socket c = new Socket(SOCKET_SERVER_ADRESS, GAME_SOCKET_PORT)) {
            ObjectInputStream ois = new ObjectInputStream(c.getInputStream());
            i = (GameInstance) ois.readObject();
        }
        return i;

    }

    @Override
    public GameInstance getWinner(UUID gameInstanceID) throws RemoteException, NoSuchGameInstanceException, AbandonedGameException, GameDrawException {
        return handler.getWinner(gameInstanceID);
    }

    @Override
    public void declareWinner(Player p, GameInstance instance) throws RemoteException {
        handler.declareWinner(p, instance);
    }

    @Override
    public void requestLogout(Player p, UUID gameInstanceID) throws RemoteException {
        handler.requestLogout(p, gameInstanceID);
    }

    @Override
    public LinkedList<GameInstance> getInstances() throws RemoteException {
        return handler.getInstances();
    }

    @Override
    public void setGameDraw(UUID gameInstanceID) throws RemoteException {
        handler.setGameDraw(gameInstanceID);
    }

}
