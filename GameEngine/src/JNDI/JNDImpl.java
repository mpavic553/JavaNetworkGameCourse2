/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JNDI;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Hashtable;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author Pig3on
 */
public class JNDImpl {

    private static final String START_CLASS_LOCATION = "../PlayerClient/Resources";

    private static Hashtable env = new Hashtable();

    public static File getFile(String fileName) throws NamingException {

        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.fscontext.RefFSContextFactory");
        env.put(Context.PROVIDER_URL, "file:" + START_CLASS_LOCATION);

        Context context = new InitialContext(env);

        Object fileObj = context.lookup(fileName);

        File startFile = (File) fileObj;

        return startFile;

    }

    public static String readFile(File sentFile) throws FileNotFoundException, IOException {

        StringBuilder build = new StringBuilder();

        try (BufferedReader br = new BufferedReader(new FileReader(sentFile))) {

            String line = br.readLine();

            while (line != null) {
                build.append(line);
                build.append(System.lineSeparator());
                line = br.readLine();

            }

            return build.toString();

        }
    }

}
