/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xmlParse;

import Engine.GameInstance;
import Engine.Player;
import Engine.Question;
import com.lowagie.text.DocumentException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.UUID;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Attr;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xhtmlrenderer.pdf.ITextRenderer;

import org.xml.sax.SAXException;



/**
 *
 * @author Pig3on
 */
public class XMLParserImpl {
        private static final String REPORTSFOLDER = "./Reports/";
        private static final String ASSETSFOLDER = "./Assets/";
        private static final String REPORTSHTML = "./ReportsHTML/";

    public static LinkedList<Question> getQuestionsFromXMl(File xmlQuestions) throws ParserConfigurationException, SAXException, IOException {

        LinkedList<Question> questions = new LinkedList<>();

        DocumentBuilder docBuild = getDocumentBuilder();

        Document docXMLDOM = docBuild.parse(xmlQuestions);

        Node root = docXMLDOM.getDocumentElement();

        NodeList questionList = root.getChildNodes();

        if (questionList.getLength() > 0) {

            for (int i = 0; i < questionList.getLength(); i++) {
                Node currentNode = questionList.item(i);

                
                Question q = new Question();
                q.setQuestionID(getQuestionID(currentNode));

                Node questionText = currentNode.getFirstChild();
                q.setQuestionText(((Text) questionText.getFirstChild()).getWholeText());

                Node questionAnswer = questionText.getNextSibling();
                q.setQuestionSolution(((Text) questionAnswer.getFirstChild()).getWholeText());

                questions.add(q);

            }
        }

        return questions;

    }

    private static DocumentBuilder getDocumentBuilder() throws ParserConfigurationException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setIgnoringElementContentWhitespace(true);
        factory.setIgnoringComments(true);

        DocumentBuilder build = factory.newDocumentBuilder();

        return build;

    }

    private static UUID getQuestionID(Node currentNode) {
        NamedNodeMap map = currentNode.getAttributes();

        Attr a = (Attr) map.item(0);

        return UUID.fromString(a.getValue());
    }
    
    
    
    
    public static void ExportInstanceToXML(GameInstance instance) throws ParserConfigurationException, TransformerConfigurationException, TransformerException, FileNotFoundException, IOException, DocumentException{
        
        DocumentBuilder build = getDocumentBuilder();
        for (Player player : instance.getPlayerList()) {
            
            
            DOMImplementation implement = build.getDOMImplementation();
            
            Document playerReport = implement.createDocument(null, "Player", null);
                    
            Element root = playerReport.getDocumentElement();
            root.setAttribute("PlayerID", player.getPlayerID().toString());
            
            Element gameID = playerReport.createElement("GameInstanceID");
            Text gameIDText = playerReport.createTextNode(instance.getGameInstanceID().toString());
            gameID.appendChild(gameIDText);
            
            
            Element Question = playerReport.createElement("Question");
            Text Questiontext = playerReport.createTextNode(instance.getQuestion().getQuestionText());
            Question.appendChild(Questiontext);
            
            Element username = playerReport.createElement("Username");
            Text usernametext = playerReport.createTextNode(player.getUsername());
            username.appendChild(usernametext);
            
            
            Element solution = playerReport.createElement("Solution");
            
            Element source = playerReport.createElement("Source");
            Text sourcetext = playerReport.createTextNode(player.getPlayerSolution().getSourceCode());
            source.appendChild(sourcetext);
            
            Element ElapsedTime = playerReport.createElement("ElapsedTime");
            Text ElapsedTimeText = playerReport.createTextNode(Long.toString(player.getPlayerSolution().getElapsedTime()));
            ElapsedTime.appendChild(ElapsedTimeText);
            
            solution.appendChild(source);
            solution.appendChild(ElapsedTime);
            
            Element winner = playerReport.createElement("Winner");
            if(instance.getWinnner().getPlayerID().equals(player.getPlayerID())){
                Text winnerText = playerReport.createTextNode("1");
                winner.appendChild(winnerText);
            }
            else{
                    Text winnerText = playerReport.createTextNode("0");
                winner.appendChild(winnerText);
                    }
            
            Element judgeComment = playerReport.createElement("JudgeComment");
            if(player.getDescription() != null){
                
            Text judgeCommentText = playerReport.createTextNode(player.getDescription());
            judgeComment.appendChild(judgeCommentText);
            
            }
            else{
                Text judgeCommentText = playerReport.createTextNode("");
                judgeComment.appendChild(judgeCommentText);
            }
            
            root.appendChild(gameID);
            root.appendChild(Question);
            root.appendChild(username);
            root.appendChild(solution);
            root.appendChild(winner);
            root.appendChild(judgeComment);
            String documentName = player.getUsername() + " " + LocalDate.now() + ".xml";
            File f = new File("./Reports");
            
            if(!f.exists()){
                f.mkdir();
            }
            File xml = new File(f, documentName);
            
            writeToXMl(playerReport,xml);
            
            RenderPDF(xml);
            
        }
        
        
       
        
        
        
    }

    private static void writeToXMl(Document playerReport, File f) throws TransformerConfigurationException, TransformerException {
        
        
        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2"); 
        
        DOMSource source = new DOMSource(playerReport);
        
        StreamResult result = new StreamResult(f);
        transformer.transform(source, result);
 
    }
    
    
    public static void RenderPDF(File xmlFile) throws TransformerConfigurationException, FileNotFoundException, TransformerException, IOException, DocumentException{

        TransformerFactory fact = TransformerFactory.newInstance();
        Transformer trans = fact.newTransformer( new StreamSource(ASSETSFOLDER +"Template.xsl"));
        
        String FilenameWithoutExt = xmlFile.getName().replaceFirst("[.][^.]+$", "");
        
        File html = new File(REPORTSHTML+FilenameWithoutExt+".html");
        
        trans.transform(new StreamSource(xmlFile), new StreamResult(new FileOutputStream(html)));
        
        File pdf = new File(REPORTSFOLDER+FilenameWithoutExt+".pdf");
        try(OutputStream os = new FileOutputStream(pdf)){
            
           ITextRenderer renderer = new ITextRenderer();
           renderer.setDocument(html);
           renderer.layout();
           renderer.createPDF(os);
           
        }
 
    }
    
    
   

}
