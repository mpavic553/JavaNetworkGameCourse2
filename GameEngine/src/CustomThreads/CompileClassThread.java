/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CustomThreads;

import CustomExceptions.InlineCompilerException;
import DML.SolutionHandler;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.concurrent.Callable;

/**
 *
 * @author Pig3on
 */
public class CompileClassThread implements Callable<Void> {

    private final SolutionHandler handler;
    private final String sourceCode;

    public CompileClassThread(SolutionHandler handler, String sourceCode) {
        this.handler = handler;
        this.sourceCode = sourceCode;
    }

    @Override
    public Void call() throws InlineCompilerException, IOException, InterruptedException {

        handler.compileSolution(sourceCode);

        System.out.println("Compiler is done Time: " + LocalDateTime.now().toString());
        return null;
    }

}
