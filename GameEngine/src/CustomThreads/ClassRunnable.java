/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CustomThreads;

import DML.SolutionHandler;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pig3on
 */
public class ClassRunnable implements Runnable {

    private final SolutionHandler handler;

    public ClassRunnable(SolutionHandler handler) {
        this.handler = handler;

    }

    @Override
    public void run() {

        try {
            System.out.println("Run started at: " + LocalDateTime.now().toString());
            handler.startInstance();

        } catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | MalformedURLException ex) {
            Logger.getLogger(ClassRunnable.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            System.out.println("\u001B[31mBUILD FAILED at: " + LocalDateTime.now().toString());
        }

    }

}
