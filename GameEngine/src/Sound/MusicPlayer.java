/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sound;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javax.naming.NamingException;

/**
 *
 * @author Pig3on
 */
public class MusicPlayer {
    private static MediaPlayer player;
    private MusicPlayer(){
        
    }
    
    public static void play(String source) throws NamingException{
        
            player = new MediaPlayer(new Media(JNDI.JNDImpl.getFile(source).toURI().toString()));
            
        
        player.play();
    }
    public static void resume(){
        if(player !=null){
           player.play();
        }
      
    }
    public static void stop(){
        if(player !=null){
            player.stop();
        }
       
    }
}
