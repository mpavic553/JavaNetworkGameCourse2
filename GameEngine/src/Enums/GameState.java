/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Enums;

import java.io.Serializable;

/**
 *
 * @author Pig3on
 */
public enum GameState implements Serializable {
    
    STARTGAME,
    WAITP2,
    REVIEW_READY,
    TAKEN_FOR_REVIEW,
    GAME_OVER,
    ABANDONED,
    DRAW
}
