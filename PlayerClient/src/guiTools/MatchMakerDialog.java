/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guiTools;

import Engine.GameInstance;
import java.util.LinkedList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.ListView;
import javafx.scene.layout.BorderPane;
import javafx.util.Callback;

/**
 *
 * @author Pig3on
 */
public class MatchMakerDialog extends Dialog<String> {

    LinkedList<GameInstance> instances;

    public MatchMakerDialog(LinkedList<GameInstance> instances) {
        
        this.setTitle("Pick a game to play in");
        this.instances = instances;
        ButtonType mactmakingButtonType = new ButtonType("Play", ButtonData.OK_DONE);
        this.getDialogPane().getButtonTypes().addAll(mactmakingButtonType, ButtonType.CANCEL);
        BorderPane borderPane = new BorderPane();
        ListView<GameInstance> listView = new ListView<>();

        ObservableList<GameInstance> observeList = FXCollections.observableArrayList(instances);
        listView.setItems(observeList);

        borderPane.getChildren().add(listView);

        this.getDialogPane().setContent(listView);

        listView.setPrefSize(600, 300);

        this.setResultConverter(new Callback<ButtonType, String>() {
            @Override
            public String call(ButtonType param) {
                if (param == mactmakingButtonType) {
                    if (!instances.isEmpty()) {
                        if (listView.getSelectionModel().isEmpty()) {
                            return null;
                        } else {
                            return listView.getSelectionModel().getSelectedItem().getGameInstanceID().toString();
                        }
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }

            }
        });

    }

}
