/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guiTools;

import com.google.common.base.Stopwatch;
import java.util.concurrent.TimeUnit;
import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.scene.control.Label;

/**
 *
 * @author Pig3on
 */
public class StopwatchTimer extends AnimationTimer {

    private Stopwatch stopwatch;
    private Label l;

    public StopwatchTimer(Stopwatch stopwatch, Label l) {
        this.stopwatch = stopwatch;
        this.l = l;
    }

    @Override
    public void handle(long now) {
        Platform.runLater(() -> l.setText(stopwatch.toString()));
    }

    @Override
    public void start() {
        stopwatch.start();
        super.start();

    }

    public void startCountringStopwatch() {
        stopwatch.start();
    }

    @Override
    public void stop() {

        if (stopwatch.isRunning()) {
            stopwatch.stop();
        }

        super.stop();

    }

    public void stopCountingStopwatch() {
        stopwatch.stop();

    }

    public long getElapsedTime() {
        return stopwatch.elapsed(TimeUnit.MILLISECONDS);
    }

}
