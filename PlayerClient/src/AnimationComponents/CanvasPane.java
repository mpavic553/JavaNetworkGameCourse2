/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AnimationComponents;

import java.util.LinkedList;
import javafx.beans.value.ObservableValue;
import javafx.scene.canvas.Canvas;
import javafx.scene.layout.Pane;

/**
 *
 * @author Pig3on
 */
public class CanvasPane extends Pane {

    private final Canvas canvas;

    private final CanvasAnimator animator;

    public CanvasPane() {

        canvas = new Canvas();
        canvas.setWidth(this.getWidth());
        canvas.setHeight(this.getHeight());
        this.getChildren().add(canvas);
        animator = new CanvasAnimator(this);

        this.widthProperty().addListener((ObservableValue<? extends Number> observable, Number oldWidth, Number newWidth) -> {
            canvas.setWidth(newWidth.intValue());
           
        });
        this.heightProperty().addListener((ObservableValue<? extends Number> observable, Number oldHeighjt, Number newHeight) -> {
            canvas.setHeight(newHeight.intValue());
         
        });

        animator.start();
    }

    public void setAnimation(LinkedList<AnimatedImage> images) {
        animator.setAnimatedImages(images);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        animator.stop();
    }

    public Canvas getCanvas() {
        return canvas;
    }

}
