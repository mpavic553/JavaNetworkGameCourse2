package AnimationComponents;

import javafx.scene.image.Image;
import javafx.scene.paint.Color;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Pig3on
 */
public class AnimatedImage {

    private Image[] frames;
    private double duration;
    private Color fillrect = null;

    public void setFillRect(Color fillrect) {
        this.fillrect = fillrect;
    }

    public Color getFillRect() {
        return fillrect;
    }

    public Image[] getFrames() {
        return frames;
    }

    public void setFrames(Image[] frames) {
        this.frames = frames;
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }

    public Image getFrame(double time) {
        int index = (int) ((time % (frames.length * duration)) / duration);
        return frames[index];
    }
}
