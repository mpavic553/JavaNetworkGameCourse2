/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AnimationComponents;

import java.util.LinkedList;
import javafx.scene.control.Button;

/**
 *
 * @author Pig3on
 */
public class AnimatedButton extends Button {

    private ButtonAnimator animator;

    public LinkedList<AnimatedImage> getImage() {
        return animator.image;
    }

    public void setImage(LinkedList<AnimatedImage> image) {
        animator.setImage(image);

    }

    public AnimatedButton() {
        animator = new ButtonAnimator(this);
        this.setOnMouseEntered((event) -> {

            animator.start();

        });

        this.setOnMouseExited((event) -> {
            animator.stop();
        });
    }

}
