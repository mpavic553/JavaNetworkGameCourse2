/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AnimationComponents;

import java.util.LinkedList;
import javafx.scene.canvas.GraphicsContext;

/**
 *
 * @author Pig3on
 */
public class CanvasAnimator extends Animator {

    private final CanvasPane canvasPane;
    private final GraphicsContext gc;
    LinkedList<AnimatedImage> animatedImages;

    public CanvasAnimator(CanvasPane canvasPane) {
        this.canvasPane = canvasPane;
        gc = canvasPane.getCanvas().getGraphicsContext2D();
        animatedImages = new LinkedList<>();

    }

    public void setAnimatedImages(LinkedList<AnimatedImage> animatedImages) {
        this.animatedImages = animatedImages;
    }

    @Override
    public void handleAnimation(double t) {
        if (animatedImages != null) {
            double widthImage = canvasPane.getCanvas().getWidth();
            double heightImage = canvasPane.getCanvas().getHeight();

            for (AnimatedImage animatedImage : animatedImages) {

                if (animatedImage.getFillRect() != null) {
                    gc.setFill(animatedImage.getFillRect());
                    gc.fillRect(0, 0, (int) widthImage, (int) heightImage);
                } else if (animatedImage.getFrames().length == 1) {
                    gc.drawImage(animatedImage.getFrames()[0], 0, 0, (int) widthImage, (int) heightImage);
                } else {
                    gc.drawImage(animatedImage.getFrame(t), 0, 0, (int) widthImage, (int) heightImage);
                }

            }
        }

    }

}
