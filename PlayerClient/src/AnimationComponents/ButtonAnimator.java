/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AnimationComponents;

import javafx.animation.AnimationTimer;
import javafx.scene.control.Button;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;

/**
 *
 * @author Pig3on
 */
public class ButtonAnimator extends Animator {

    private final Button animatedButton;

    public ButtonAnimator(Button animatedButton) {

        this.animatedButton = animatedButton;

    }

    @Override
    public void handleAnimation(double t) {
        if (image != null) {
            for (AnimatedImage animatedImage : image) {

                BackgroundImage imageback = new BackgroundImage(animatedImage.getFrame(t),
                        BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT,
                        BackgroundPosition.CENTER,
                        BackgroundSize.DEFAULT);

                Background back = new Background(imageback);

                animatedButton.setBackground(back);
            }

        }
    }

}
