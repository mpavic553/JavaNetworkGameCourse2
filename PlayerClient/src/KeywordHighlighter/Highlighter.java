/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordHighlighter;

import javafx.application.Platform;

/**
 *
 * @author Pig3on
 */
public class Highlighter implements Runnable {

    KeywordHighlight highlight;
    

    public Highlighter(KeywordHighlight highlight) {
        this.highlight = highlight;
    }

    @Override
    public void run() {
       Platform.runLater(() ->highlight.highlightWords());
    }
    
}
