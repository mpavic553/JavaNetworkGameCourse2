/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordHighlighter;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.naming.NamingException;
import org.fxmisc.richtext.CodeArea;
import org.fxmisc.richtext.model.StyleSpans;
import org.fxmisc.richtext.model.StyleSpansBuilder;

/**
 *
 * @author Pig3on
 */
public class KeywordHighlight {
    
    CodeArea area;
  
    private static  List<String> keywords;
    
      
    
    private static  String KEYWORD_PATTERN;
    private static  Pattern PATTERN;
    
    private static final String BRACE_PATTERN = "\\{|\\}";
    private static final String BRACKET_PATTERN = "\\[|\\]";

    
    
    public KeywordHighlight(CodeArea area) throws NamingException, IOException {
        this.area = area;
        String s = JNDI.JNDImpl.readFile((JNDI.JNDImpl.getFile("JavaKeywords.txt")));
        
        keywords = Arrays.asList(s.split(","));
        
        KEYWORD_PATTERN = "\\b("+ String.join("|", keywords)+")\\b";
        PATTERN = Pattern.compile("(?<keyword>" +KEYWORD_PATTERN + ")"
                                + "|(?<braces>" + BRACE_PATTERN +")"
                                + "|(?<brackets>" + BRACKET_PATTERN +")");
       
    }
    

    public void highlightWords() {
   
        area.setStyleSpans(0, calculateKeywords(area.getText()));
    }
    
    private StyleSpans<Collection<String>> calculateKeywords(String sourceText){
        Matcher matcher = PATTERN.matcher(sourceText);
       
        int lastkwEnd = 0;
        
        StyleSpansBuilder build = new StyleSpansBuilder<>();
        
        while(matcher.find()){
          
            String styleClass =
                    matcher.group("keyword") != null ? "keyword":
                    matcher.group("braces") !=null ? "brace":
                    matcher.group("brackets") !=null ? "bracket":
                    null;
            
            
            build.add(Collections.emptyList(),matcher.start() - lastkwEnd);
            build.add(Collections.singleton(styleClass),matcher.end() - matcher.start());
            lastkwEnd = matcher.end();
         
           
        }
        
        build.add(Collections.emptyList(),sourceText.length() - lastkwEnd);
        
        return build.create();
    }

   
    
}
