/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package playerClientGUI;

import Sound.MusicPlayer;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javax.naming.NamingException;

/**
 *
 * @author Pig3on
 */
public class ProgrammerClient extends Application {

    private Stage ParentStage;

    @Override
    public void start(Stage stage) throws IOException {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("ProgrammerFXML.fxml"));
        Parent root = loader.load();

        Scene scene = new Scene(root);

        ProgrammerFXMLController programmerController = (ProgrammerFXMLController) loader.getController();

        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {

                try {
                    programmerController.shutdown();
                    MusicPlayer.play("flash.mp3");
                    ParentStage.show();
                    stage.close();
                } catch (RemoteException ex) {
                    System.out.println(ex.getMessage());
                    ParentStage.show();
                    stage.close();
                } catch (NamingException ex) {
                    Logger.getLogger(ProgrammerClient.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        });

        stage.setScene(scene);
        stage.show();
    }

    public ProgrammerClient(Stage ParentStage) {
        this.ParentStage = ParentStage;
    }

}
