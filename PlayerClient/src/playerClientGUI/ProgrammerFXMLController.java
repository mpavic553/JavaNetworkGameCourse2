/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package playerClientGUI;

import AnimationComponents.AnimatedButton;
import AnimationComponents.AnimatedImage;
import AnimationHelper.SpriteSheetCutter;
import CustomExceptions.ExitGameException;
import CustomExceptions.AbandonedGameException;
import Engine.GameManager;
import Engine.Move;
import CustomExceptions.InlineCompilerException;
import CustomExceptions.LobbyFullException;
import Engine.Player;
import JNDI.JNDImpl;
import KeywordHighlighter.Highlighter;
import KeywordHighlighter.KeywordHighlight;

import com.google.common.base.Stopwatch;

import guiTools.ClockThread;
import guiTools.MatchMakerDialog;
import SpamThreads.RunWaitThread;
import guiTools.StopwatchTimer;
import SpamThreads.WinnerListener;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputDialog;
import javafx.stage.FileChooser;
import javax.naming.NamingException;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;
import org.fxmisc.richtext.CodeArea;
import org.fxmisc.richtext.LineNumberFactory;

/**
 *
 * @author Pig3on
 */
public class ProgrammerFXMLController implements Initializable {

    private GameManager mngr;
    private final LinkedList<Move> moves = new LinkedList<>();
    private final Alert alert = new Alert(Alert.AlertType.ERROR);
    private final Timer clockTimer = new Timer();
    private MatchMakerDialog dialog;
    private final TextInputDialog inputUsernameDialog = new TextInputDialog("username");
    private String playerUsername;
    private StopwatchTimer stopwatch;

    private KeywordHighlight highlight;

    @FXML
    private Label lblClock;
    @FXML
    private Label lblStopWatch;
    @FXML
    private AnimatedButton btnSubmit;
    @FXML
    private AnimatedButton btnLoadClass;
    @FXML
    private CodeArea txtArea;
    @FXML
    private CodeArea errorArea;
    @FXML
    private Label txtUsername;
    @FXML
    private CodeArea QuestionArea;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        errorArea.setWrapText(true);
        QuestionArea.setWrapText(true);

        try {
            runKeywordHighlighting();
            setStartText();
            runClock();
            guiSetup();
            setupInitialGameStartup();
            setupWaitP2();

        } catch (RemoteException | NotBoundException | LobbyFullException ex) {
            Logger.getLogger(ProgrammerFXMLController.class.getName()).log(Level.SEVERE, null, ex);
            alert.setHeaderText("The server is offline");
            alert.setContentText("Server Appears to be offline");
            alert.showAndWait();
            System.exit(0);
        } catch (ExitGameException ex) {
            alert.setHeaderText(ex.getMessage());
            alert.setContentText("Okay bye");
            alert.showAndWait();
            System.exit(0);
        } catch (IOException | NamingException ex) {
            Logger.getLogger(ProgrammerFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void runKeywordHighlighting() throws NamingException, IOException {
        highlight = new KeywordHighlight(txtArea);
        txtArea.richChanges()
                .filter(ch -> !ch.getInserted().equals(ch.getRemoved()))
                .subscribe(change -> {
                    Highlighter highlighter = new Highlighter(highlight);
                    Thread highligherThread = new Thread(highlighter);
                    highligherThread.setDaemon(true);
                    highligherThread.setName("highlighter");
                    highligherThread.start();

                });
    }

    private void setStartText() {

        try {
            File f = JNDI.JNDImpl.getFile("SolutionStart.bin");

            txtArea.replaceText(JNDI.JNDImpl.readFile(f));

        } catch (NamingException ex) {
            Logger.getLogger(ProgrammerFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProgrammerFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void runClock() {
        clockTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Thread t = new Thread(new ClockThread(lblClock));
                t.setDaemon(true);
                Platform.runLater(t);

            }
        }, 0, 1000);
    }

    private void guiSetup() throws IOException, NamingException {
        txtArea.setEditable(true);
        errorArea.setEditable(false);
        txtArea.setParagraphGraphicFactory(LineNumberFactory.get(txtArea));
        stopwatch = new StopwatchTimer(Stopwatch.createUnstarted(), lblStopWatch);
        txtArea.focusedProperty().addListener(new ChangeListener<Boolean>() {
            boolean once = false;

            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {

                if (newValue && !once) {
                    startCounting();

                    once = true;
                }
            }
        });

        setupAnimation();
    }

    private void startCounting() {

        stopwatch.start();

    }

    private void setupAnimation() throws IOException, NamingException {
        AnimatedImage buttonAnimation = new AnimatedImage();
        buttonAnimation.setFrames(SpriteSheetCutter.getImages(256, 44, JNDImpl.getFile("LightningButton.png")));
        buttonAnimation.setDuration(0.100);

        LinkedList<AnimatedImage> images = new LinkedList<>();

        images.add(buttonAnimation);
        btnSubmit.setImage(images);
        btnLoadClass.setImage(images);

    }

    private void setupInitialGameStartup() throws RemoteException, NotBoundException, LobbyFullException, ExitGameException {
        playerUsername = getPlayerUsername();

        txtUsername.setText(playerUsername);
        mngr = new GameManager(new Player(playerUsername));

        UUID gameID = getGameInstanceID();
        mngr.startGame(gameID);
        QuestionArea.replaceText(mngr.getCurrentGameInstance().getQuestion().getQuestionText());
        QuestionArea.appendText("\n EXAMPLE:");
        QuestionArea.appendText("\n" + mngr.getCurrentGameInstance().getQuestion().getQuestionSolution());

    }

    private String getPlayerUsername() throws ExitGameException {
        inputUsernameDialog.setTitle("Select your username");
        inputUsernameDialog.setHeaderText("Please type your username(CASE SENSITIVE)");
        Optional<String> result = inputUsernameDialog.showAndWait();

        if (result.isPresent()) {
            return result.get();

        } else {
            throw new ExitGameException();
        }
    }

    private void setupWaitP2() {

        setDisabledState("Please wait for player 2");
        Thread t = new Thread(new RunWaitThread(mngr, btnSubmit, btnLoadClass, txtArea, errorArea));
        t.setDaemon(true);

        t.start();

    }

    private UUID getGameInstanceID() throws RemoteException, ExitGameException {
        dialog = new MatchMakerDialog(mngr.getInstances());

        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {

            return UUID.fromString(result.get());

        }
        return null;

    }

    @FXML
    private void loadClassAction(ActionEvent event) {

        try {

            mngr.runSolution(txtArea.getText());

        } catch (InterruptedException | ClassNotFoundException ex) {
            Logger.getLogger(ProgrammerFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InlineCompilerException ex) {
            printDIagnostics(ex);
        }

    }

    @FXML
    private void submitCodeAction(ActionEvent event) {

        stopwatch.stopCountingStopwatch();

        try {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Save solution");
            File load = fileChooser.showSaveDialog(txtArea.getScene().getWindow());
            if (load != null) {
                mngr.submitCode(txtArea.getText(), moves, load, stopwatch.getElapsedTime());

                setDisabledState("Please wait for a judge to review your soluion");
                startListeningForWinner();

            }

        } catch (InterruptedException | ClassNotFoundException | InstantiationException | IllegalAccessException | IOException ex) {
            Logger.getLogger(ProgrammerFXMLController.class.getName()).log(Level.SEVERE, null, ex);
            if (ex instanceof RemoteException) {
                alert.setHeaderText("The server is offline");
                alert.setContentText("Server Appears to be offline");
                alert.showAndWait();
            }

        } catch (InlineCompilerException ex) {
            printDIagnostics(ex);
            stopwatch.startCountringStopwatch();
        } catch (AbandonedGameException ex) {
            errorArea.replaceText("Other player abandoned the match,You can leave this match!");
        }

    }

    private void printDIagnostics(InlineCompilerException ex) {
        List<Diagnostic<? extends JavaFileObject>> diagnostic = ex.getDiagnosics();

        for (Diagnostic<? extends JavaFileObject> diagnosticLine : diagnostic) {

            errorArea.replaceText("Error on line: " + diagnosticLine.getLineNumber() + ":  " + diagnosticLine.getMessage(Locale.ENGLISH));

        }
    }

    private void startListeningForWinner() {
        Thread t = new Thread(new WinnerListener(mngr, errorArea, mngr.getCurrentPlayer()));
        t.start();
    }

    private void setDisabledState(String msg) {
        btnSubmit.setDisable(true);
        btnLoadClass.setDisable(true);
        txtArea.setFocusTraversable(false);
        txtArea.setEditable(false);
        errorArea.replaceText(msg);
    }

    public void shutdown() throws RemoteException {
        clockTimer.cancel();

        mngr.requestLogout();

        stopwatch.stop();
    }

}
