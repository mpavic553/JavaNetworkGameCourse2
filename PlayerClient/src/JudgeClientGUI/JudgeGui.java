/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JudgeClientGUI;

import Sound.MusicPlayer;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javax.naming.NamingException;
import playerClientGUI.ProgrammerFXMLController;

/**
 *
 * @author Pig3on
 */
public class JudgeGui extends Application {

    Stage parentSage;

    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("JudgeGUIFXML.fxml"));
        Parent root = loader.load();

        Scene scene = new Scene(root);

 

        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {

                
                parentSage.show();
                try {
                    MusicPlayer.play("flash.mp3");
                } catch (NamingException ex) {
                    Logger.getLogger(JudgeGui.class.getName()).log(Level.SEVERE, null, ex);
                }
                stage.close();

            }
        });

        stage.setScene(scene);
        stage.show();

    }

    public JudgeGui(Stage parentSage) {
        this.parentSage = parentSage;
    }

}
