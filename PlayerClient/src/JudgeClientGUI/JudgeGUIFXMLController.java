/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JudgeClientGUI;

import AnimationComponents.AnimatedButton;
import AnimationComponents.AnimatedImage;
import AnimationHelper.SpriteSheetCutter;
import CustomExceptions.GameDrawException;
import CustomExceptions.InlineCompilerException;
import Engine.GameInstance;
import Engine.GameManager;
import Engine.Player;
import JNDI.JNDImpl;
import KeywordHighlighter.Highlighter;
import KeywordHighlighter.KeywordHighlight;
import com.lowagie.text.DocumentException;
import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.paint.Color;
import javax.naming.NamingException;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.fxmisc.richtext.CodeArea;
import org.fxmisc.richtext.LineNumberFactory;

/**
 * FXML Controller class
 *
 * @author Pig3on
 */
public class JudgeGUIFXMLController implements Initializable {

    private GameManager gameManager;
    private GameInstance currentGameInstance;
    private Player currentReviewedPlayer;
    private final List<Player> disqualifiedPlayers = new ArrayList<>();
    private final Alert alertNoGame = new Alert(Alert.AlertType.WARNING);
    KeywordHighlight highlight;
    @FXML
    private AnimatedButton btnGameGameInstance;
    @FXML
    private AnimatedButton btnSetGood;
    @FXML
    private AnimatedButton btnSetBad;
    @FXML
    private Label lblGameInstanceID;
    @FXML
    private Label lblPlayerTimeID;
    @FXML
    private AnimatedButton btnRunSolution;
    @FXML
    private CodeArea txtDebuggerOutput;
    @FXML
    private TextArea txtDescription;
    @FXML
    private CodeArea txtCodeInput;
    @FXML
    private CodeArea txtQuestionsArea;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            runKeywordHighlighting();
            gameManager = new GameManager();
            setupAnimation();
            disableControlButtons();

            txtCodeInput.setParagraphGraphicFactory(LineNumberFactory.get(txtCodeInput));

        } catch (RemoteException ex) {
            Logger.getLogger(JudgeGUIFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NotBoundException | NamingException ex) {
            Logger.getLogger(JudgeGUIFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(JudgeGUIFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void runKeywordHighlighting() throws NamingException, IOException {
        highlight = new KeywordHighlight(txtCodeInput);
        txtCodeInput.richChanges()
                .filter(ch -> !ch.getInserted().equals(ch.getRemoved()))
                .subscribe(change -> {
                    Highlighter highlighter = new Highlighter(highlight);
                    Thread highligherThread = new Thread(highlighter);
                    highligherThread.setDaemon(true);
                    highligherThread.setName("highlighter");
                    highligherThread.start();

                });
    }

    private void setupAnimation() throws NamingException, IOException {
        AnimatedImage buttonAnimation = new AnimatedImage();
        buttonAnimation.setFrames(SpriteSheetCutter.getImages(256, 44, JNDImpl.getFile("LightningButton.png")));
        buttonAnimation.setDuration(0.100);
        LinkedList<AnimatedImage> images = new LinkedList<>();

        images.add(buttonAnimation);
        btnGameGameInstance.setImage(images);
        btnRunSolution.setImage(images);
        btnSetBad.setImage(images);
        btnSetGood.setImage(images);

    }

    private void disableControlButtons() {
        btnRunSolution.setDisable(true);
        btnSetBad.setDisable(true);
        btnSetGood.setDisable(true);
    }

    @FXML
    private void btnClickGetInstance(ActionEvent event) {
        try {
            currentGameInstance = gameManager.getNextGameInstance();
            fillGameData();
            enableControlButtons();

        } catch (IOException ex) {
            if (ex instanceof EOFException) {
                alertNoGame.setTitle("No games");
                alertNoGame.setContentText("No games currently available for review");
                alertNoGame.setHeaderText("No games currently available");
                alertNoGame.showAndWait();
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(JudgeGUIFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (GameDrawException ex) {
            gameDrawException();

        }

    }

    private void fillGameData() throws GameDrawException {
        lblGameInstanceID.setText(currentGameInstance.getGameInstanceID().toString());
        txtQuestionsArea.replaceText(currentGameInstance.getQuestion().getQuestionText());
        txtQuestionsArea.appendText("\n" + currentGameInstance.getQuestion().getQuestionSolution());
        Player p = getFasterPlayer();

        setPlayerDataForReview(p);

    }

    private void setPlayerDataForReview(Player p) {
        currentReviewedPlayer = p;
        txtCodeInput.replaceText(p.getPlayerSolution().getSourceCode());
        lblPlayerTimeID.setText(Long.toString(p.getPlayerSolution().getElapsedTime()));
    }

    private void enableControlButtons() {
        btnRunSolution.setDisable(false);
        btnSetBad.setDisable(false);
        btnSetGood.setDisable(false);
    }

    private void gameDrawException() {
        alertNoGame.setTitle("Game is a draw");
        alertNoGame.setHeaderText("Game is a draw");
        alertNoGame.setContentText("A game result is draw");
        alertNoGame.showAndWait();
    }

    private Player getFasterPlayer() throws GameDrawException {

        Player p = null;
        Long minTime = getStarterTime();

        if (minTime == null) {
            setGameDraw();

        }

        for (Player player : currentGameInstance.getPlayerList()) {
            if (!disqualifiedPlayers.isEmpty()) {
                for (Player disqualifiedPlayer : disqualifiedPlayers) {
                    if (!player.getPlayerID().equals(disqualifiedPlayer.getPlayerID())) {

                        if (player.getPlayerSolution().getElapsedTime() <= minTime) {

                            minTime = player.getPlayerSolution().getElapsedTime();
                            p = player;
                        }

                    }

                }
            } else {

                if (player.getPlayerSolution().getElapsedTime() <= minTime) {

                    minTime = player.getPlayerSolution().getElapsedTime();
                    p = player;
                }

            }

        }
        return p;

    }

    private Long getStarterTime() {
        if (disqualifiedPlayers.isEmpty()) {
            return currentGameInstance.getPlayerList().getFirst().getPlayerSolution().getElapsedTime();
        }
        Long time = null;

        if (currentGameInstance.getPlayerList().size() == disqualifiedPlayers.size()) {
            return time;
        }
        for (Player player : currentGameInstance.getPlayerList()) {

            for (Player disqualifiedPlayer : disqualifiedPlayers) {

                if (!disqualifiedPlayer.getPlayerID().equals(player.getPlayerID())) {
                    return player.getPlayerSolution().getElapsedTime();
                }

            }

        }

        return time;
    }

    private void setGameDraw() throws GameDrawException {

        try {
            gameManager.setGameDraw(currentGameInstance.getGameInstanceID());
            clearBoard();
            throw new GameDrawException();
        } catch (RemoteException ex) {
            Logger.getLogger(JudgeGUIFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    private void btnClickGood(ActionEvent event) {
        try {
            currentReviewedPlayer.setDescription(txtDescription.getText());
            currentGameInstance.setWinnner(currentReviewedPlayer);
            gameManager.declareWinner(currentReviewedPlayer, currentGameInstance);
            xmlParse.XMLParserImpl.ExportInstanceToXML(currentGameInstance);

            disableControlButtons();
            clearBoard();
        } catch (RemoteException ex) {
            Logger.getLogger(JudgeGUIFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TransformerException ex) {
            Logger.getLogger(JudgeGUIFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(JudgeGUIFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(JudgeGUIFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(JudgeGUIFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DocumentException ex) {
            Logger.getLogger(JudgeGUIFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void clearBoard() {
        txtCodeInput.replaceText("");
        txtDebuggerOutput.replaceText("");
        txtDescription.setText("");
        lblGameInstanceID.setText("");
        lblPlayerTimeID.setText("");
        txtDescription.setText("");
        txtQuestionsArea.replaceText("");
        currentGameInstance = null;
        currentReviewedPlayer = null;
        disqualifiedPlayers.clear();
    }

    @FXML
    private void btnClickBad(ActionEvent event) {
        currentReviewedPlayer.setDescription(txtDescription.getText());
        disqualifiedPlayers.add(currentReviewedPlayer);
        txtDescription.setText("");

        try {
            fillGameData();
        } catch (GameDrawException ex) {
            gameDrawException();
        }

    }

    @FXML
    private void btnClickRunSolution(ActionEvent event) {

        try {
            gameManager.runSolution(txtCodeInput.getText());
        } catch (InterruptedException ex) {
            Logger.getLogger(JudgeGUIFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(JudgeGUIFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InlineCompilerException ex) {
            List<Diagnostic<? extends JavaFileObject>> diagnosics = ex.getDiagnosics();

            for (Diagnostic<? extends JavaFileObject> diagnosic : diagnosics) {
                txtDebuggerOutput.appendText("Error at: ");
                txtDebuggerOutput.appendText(Long.toString(diagnosic.getLineNumber()));
                txtDebuggerOutput.appendText(diagnosic.getMessage(Locale.ENGLISH));
                txtDebuggerOutput.appendText(diagnosic.getKind().toString());
            }
        }

    }

}
