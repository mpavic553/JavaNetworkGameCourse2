/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AnimationHelper;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javax.imageio.ImageIO;

/**
 *
 * @author Pig3on
 */
public class SpriteSheetCutter {

    public static Image[] getImages(int imgwidth, int imgHeight, File f) throws IOException {
        List<Image> images = new LinkedList<>();

        System.out.println(f.getPath());
        BufferedImage buffImg = ImageIO.read(f);

        for (int row = 0; row < buffImg.getHeight(); row += imgHeight) {

            for (int column = 0; column < buffImg.getWidth(); column += imgwidth) {

                BufferedImage frame = buffImg.getSubimage(column, row, imgwidth, imgHeight);
                images.add(SwingFXUtils.toFXImage(frame, null));

            }

        }
        Image[] imagesArray = new Image[images.size()];

        for (int i = 0; i < images.size(); i++) {
            imagesArray[i] = images.get(i);
        }

        return imagesArray;
    }
}
