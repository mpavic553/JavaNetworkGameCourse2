/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package playTypeChooser;

import AnimationComponents.AnimatedButton;
import AnimationComponents.AnimatedImage;
import AnimationComponents.CanvasPane;
import AnimationHelper.SpriteSheetCutter;
import JNDI.JNDImpl;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import JudgeClientGUI.JudgeGui;
import Sound.MusicPlayer;

import java.util.LinkedList;
import javafx.scene.paint.Color;
import javax.naming.NamingException;
import playerClientGUI.ProgrammerClient;

/**
 * FXML Controller class
 *
 * @author Pig3on
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private AnimatedButton btnProgrammer;
    @FXML
    private AnimatedButton btnPlayJudge;

    @FXML
    private CanvasPane canvasPane;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        try {
            MusicPlayer.play("flash.mp3");
            setupButtonAnimation();
            setupCanvasAnimation();
        } catch (IOException | NamingException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void setupButtonAnimation() throws IOException, NamingException {

        AnimatedImage image = new AnimatedImage();

        image.setFrames(SpriteSheetCutter.getImages(256, 44, JNDImpl.getFile("LightningButton.png")));
        image.setDuration(0.100);
        LinkedList<AnimatedImage> images = new LinkedList<>();
        images.add(image);
        btnProgrammer.setMinSize(256, 44);
        btnPlayJudge.setMinSize(256, 44);
        btnProgrammer.setImage(images);
        btnPlayJudge.setImage(images);

    }

    private void setupCanvasAnimation() throws IOException, NamingException {

        LinkedList<AnimatedImage> images = new LinkedList<>();

        AnimatedImage fillrect = new AnimatedImage();
        fillrect.setFillRect(Color.RED);

        AnimatedImage lightning = new AnimatedImage();
        lightning.setFrames(SpriteSheetCutter.getImages(896, 504, JNDImpl.getFile("LightningMenu.png")));
        lightning.setDuration(0.100);

        AnimatedImage Logo = new AnimatedImage();
        Image[] image = new Image[1];
        image[0] = new Image(JNDImpl.getFile("Logo.png").toURI().toURL().toString());
        Logo.setFrames(image);

        images.add(fillrect);
        images.add(lightning);
        images.add(Logo);

        canvasPane.setAnimation(images);

    }

    @FXML
    private void onPlayProgrammerClick(ActionEvent event) {

        runProgrammer();

    }

    private void runProgrammer() {

        try {
            Stage parent = (Stage) btnProgrammer.getScene().getWindow();
            ProgrammerClient window = new ProgrammerClient(parent);
            Stage stage = new Stage();

            window.start(stage);
            MusicPlayer.stop();
            parent.close();

        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    private void onPlayJudgeClick(ActionEvent event) {
        runJudge();
    }

    private void runJudge() {

        Stage parent = (Stage) btnProgrammer.getScene().getWindow();
        Stage s = new Stage();

        JudgeGui window = new JudgeGui(parent);
        try {
            window.start(s);
            MusicPlayer.stop();
            parent.close();

        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
