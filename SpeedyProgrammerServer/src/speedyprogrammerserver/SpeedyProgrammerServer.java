/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package speedyprogrammerserver;

import Engine.GameInstance;
import GameNetwork.NetworkSolutionHandling;
import java.io.IOException;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.NamingException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author Pig3on
 */
public class SpeedyProgrammerServer {

    private static final String SERVER_RMI_SOLUTION_NAME = "gameNetworkImpl";
    private static final int SERVER_PORT = 5532;
    private static LinkedList<GameInstance> gameInstances = new LinkedList<>();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        try {
            //System.setProperty("java.rmi.server.logCalls", "true");
            ServerConnectionThread connectionThread = new ServerConnectionThread(gameInstances);
            Thread conThread = new Thread(connectionThread);
            conThread.setDaemon(true);
            conThread.start();
            NetworkSolutionHandling handling = new NetworkSolutionHandler(gameInstances);

            NetworkSolutionHandling stub = (NetworkSolutionHandling) UnicastRemoteObject.exportObject(handling, 0);

            Registry reg = LocateRegistry.createRegistry(SERVER_PORT);

            reg.bind(SERVER_RMI_SOLUTION_NAME, stub);

            System.out.println("Server started at " + LocalDateTime.now());
        } catch (RemoteException | AlreadyBoundException ex) {
            Logger.getLogger(SpeedyProgrammerServer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException | SAXException | IOException | NamingException ex) {
            Logger.getLogger(SpeedyProgrammerServer.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
