/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package speedyprogrammerserver;

import CustomExceptions.AbandonedGameException;
import CustomExceptions.GameDrawException;
import CustomExceptions.NoSuchGameInstanceException;
import CustomExceptions.LobbyFullException;
import Engine.GameInstance;
import Engine.Player;
import Engine.QuestionFactory;
import Enums.GameState;
import GameNetwork.NetworkSolutionHandling;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.LinkedList;
import java.util.UUID;
import javax.naming.NamingException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author Pig3on
 */
public class NetworkSolutionHandler implements NetworkSolutionHandling {

    private LinkedList<GameInstance> gameInstances;
    private QuestionFactory questionFactory;

    public NetworkSolutionHandler(LinkedList<GameInstance> gameInstances) throws ParserConfigurationException, SAXException, IOException, NamingException {
        this.gameInstances = gameInstances;
        this.questionFactory = new QuestionFactory();
    }

    @Override
    public GameInstance startGame(Player p, UUID gameInstanceID) throws RemoteException, LobbyFullException {

        if (gameInstanceID == null) {
            System.out.println("No ID recieved Starting a new game");
            return createNewGameInstance(p);

        }
        if (gameInstances.isEmpty()) {
            System.out.println("Instance list is emtpty, adding new game");

            return createNewGameInstance(p);

        } else {
            System.out.println("Instance list is NOT emtpty, searching rooms");
            for (GameInstance gameInstance : gameInstances) {
                if (gameInstance.getGameState() == GameState.WAITP2) {

                    if (gameInstance.getGameInstanceID().equals(gameInstanceID)) {

                        System.out.println("ID found in game: " + gameInstance.getGameInstanceID() + " setting player 2");

                        if (gameInstance.getPlayerList().size() < gameInstance.getMAX_PLAYERS()) {
                            gameInstance.setGameState(GameState.STARTGAME);
                            gameInstance.setPlayer(p);
                            return gameInstance;
                        } else {
                            throw new LobbyFullException("This game is currently full");
                        }

                    }
                }

            }
            System.out.println("Game ID not found creating new game");
            return createNewGameInstance(p);

        }

    }

    private GameInstance createNewGameInstance(Player p) {
        GameInstance instance = new GameInstance(p); // create a new game instance with slot 2 empty
        instance.setGameState(GameState.WAITP2);
        instance.setQuestion(questionFactory.getRandomQuestion());
        gameInstances.add(instance);
        return instance;
    }

    @Override
    public boolean isOpponentReady(UUID gameInstanceID) throws RemoteException, AbandonedGameException, NoSuchGameInstanceException {

        for (GameInstance gameInstance : gameInstances) {

            if (gameInstance.getGameInstanceID().equals(gameInstanceID)) {
                if (gameInstance.getGameState() == GameState.STARTGAME) {
                    System.out.println("game ready,starting");
                    return true;
                }
                if (gameInstance.getGameState() == GameState.ABANDONED) {
                    throw new AbandonedGameException();
                } else {

                    return false;
                }
            }

        }
        throw new NoSuchGameInstanceException();
    }

    @Override
    public void sendSolution(UUID gameInstanceID, Player p) throws RemoteException, AbandonedGameException {

        for (GameInstance gameInstance : gameInstances) {

            if (gameInstance.getGameInstanceID().equals(gameInstanceID)) {
                if (gameInstance.getGameState() == GameState.ABANDONED) {
                    System.out.println("This game has been abandoned");
                    throw new AbandonedGameException();
                }
                gameInstance.setPlayer(p);

                checkIfReadyForReview(gameInstance);

            }
        }

    }

    private void checkIfReadyForReview(GameInstance gameInstance) {

        for (Player player : gameInstance.getPlayerList()) {

            if (player.getPlayerSolution() == null) {
                System.out.println("Some players did not send soluion yet");
                return;
            }
        }
        System.out.println("All players sent solution.Ready for review!");
        gameInstance.setGameState(GameState.REVIEW_READY);
    }

    @Override
    public GameInstance getWinner(UUID gameInstanceID) throws RemoteException, NoSuchGameInstanceException, AbandonedGameException, GameDrawException {

        for (GameInstance gameInstance : gameInstances) {
            if (gameInstance.getGameInstanceID().equals(gameInstanceID)) {
                if (gameInstance.getGameState() == GameState.DRAW) {
                    throw new GameDrawException();
                }
                if (gameInstance.getGameState() == GameState.GAME_OVER) {
                    return gameInstance;
                } else if (gameInstance.getGameState() == GameState.ABANDONED) {
                    System.out.println("This game has been abandoned");
                    throw new AbandonedGameException();
                }
                return null;
            }
        }
        throw new NoSuchGameInstanceException();

    }

    @Override
    public void declareWinner(Player p, GameInstance instance) throws RemoteException {

        for (GameInstance gameInstance : gameInstances) {
            if (gameInstance.getGameInstanceID().equals(instance.getGameInstanceID())) {
                gameInstance.setGameState(GameState.GAME_OVER);
                for (Player serverPlayer : gameInstance.getPlayerList()) {

                    for (Player recievedPlayer : instance.getPlayerList()) {

                        if (serverPlayer.getPlayerID().equals(recievedPlayer.getPlayerID())) {
                            serverPlayer.setDescription(recievedPlayer.getDescription());
                        }

                    }

                }
                gameInstance.setWinnner(p);
            }
        }
    }

    @Override
    public void requestLogout(Player p, UUID gameInstanceID) throws RemoteException {

        for (GameInstance gameInstance : gameInstances) {
            if (gameInstance.getGameInstanceID().equals(gameInstanceID)) {

                for (Player player : gameInstance.getPlayerList()) {

                    if (player.getPlayerID().equals(p.getPlayerID())) {

                        gameInstance.getPlayerList().remove(player);
                        checkGameState(gameInstance);
                        if (gameInstance.getPlayerList().isEmpty()) {
                            System.out.println("Game instance is empty,deleting instance");
                            gameInstances.remove(gameInstance);
                            return;
                        }

                    }

                }
            }

        }

    }

    private void checkGameState(GameInstance gameInstance) {

        if (gameInstance.getPlayerList().size() < 2) {
            System.out.println("This game has been abandoned");
            gameInstance.setGameState(GameState.ABANDONED);
        } else {
            System.out.println("Players left in game" + gameInstance.getPlayerList().size());
            gameInstance.setMAX_PLAYERS(gameInstance.getPlayerList().size());
        }

    }

    @Override
    public LinkedList<GameInstance> getInstances() throws RemoteException {

        LinkedList<GameInstance> nonStartedInstances = new LinkedList<>();

        for (GameInstance gameInstance : gameInstances) {

            if (gameInstance.getGameState() == GameState.WAITP2) {
                nonStartedInstances.add(gameInstance);
            }
        }
        return nonStartedInstances;
    }

    @Override
    public void setGameDraw(UUID gameInstanceID) throws RemoteException {
        for (GameInstance gameInstance : gameInstances) {
            if (gameInstance.getGameInstanceID().equals(gameInstanceID)) {
                gameInstance.setGameState(GameState.DRAW);
            }
        }
    }

}
